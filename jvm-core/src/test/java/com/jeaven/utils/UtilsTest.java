package com.jeaven.utils;

import com.jeaven.rtda.heap.KMethod;
import org.junit.Test;

import java.util.List;

import static org.junit.Assert.assertEquals;

public class UtilsTest {
    @Test
    public void testUtils() {
        String methodDescriptor = "(B;C;[Ljava/lang/Object;[[C;D;)V"; //
//        String methodDescriptor = "()V";
        List<String> res = Utils.parseMethodDescriptor(methodDescriptor);
        for(String s : res) {
            System.out.println(s);
        }

        KMethod kMethod = new KMethod(1,null, methodDescriptor, 1, 2, null, null, null);
        int size = kMethod.getArgSlotSize();

        assertEquals(6, size);
    }

}
