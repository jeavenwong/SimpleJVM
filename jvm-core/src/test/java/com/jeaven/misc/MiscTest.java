package com.jeaven.misc;

import org.junit.Test;

import java.util.Objects;

import static org.junit.Assert.assertEquals;

public class MiscTest {

    @Test
    public void testOthers() {
        String name = "test1";
        boolean flag = Objects.equals("test", name);
        assertEquals(false, flag);

        Object obj = new Object();
    }

}
