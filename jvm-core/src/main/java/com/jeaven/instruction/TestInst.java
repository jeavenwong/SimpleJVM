package com.jeaven.instruction;

import com.jeaven.rtda.Frame;

// 测试指令
public class TestInst implements Instruction {
    @Override
    public void execute(Frame frame) {
        System.out.println("这个指令是测试指令...表示还没有实现...");
    }

    @Override
    public String debug(String prefix) {
        return "测试指令,表示还没有实现...";
    }
}
