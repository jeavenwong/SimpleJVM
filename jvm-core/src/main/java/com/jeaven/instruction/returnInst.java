package com.jeaven.instruction;

import com.jeaven.rtda.Frame;

// return
public class returnInst implements Instruction {
    @Override
    public void execute(Frame frame) {
        System.out.println("程序运行结束后正常退出 code = 0 ...");
    }

    @Override
    public String debug(String prefix) {
        return "return";
    }
}
