package com.jeaven.instruction;

import com.jeaven.rtda.Frame;
import com.jeaven.rtda.Slot;

// 生成常量指令
public class iconst1Inst implements Instruction {
    @Override
    public void execute(Frame frame) {
        frame.operandStack.pushInt(1, Slot.INT);
    }

    @Override
    public String debug(String prefix) {
        return "iconst_1";
    }
}
