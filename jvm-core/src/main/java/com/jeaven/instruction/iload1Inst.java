package com.jeaven.instruction;

import com.jeaven.rtda.Frame;
import com.jeaven.rtda.Slot;

// 将局部变量表中索引为1的变量入操作数栈
public class iload1Inst implements Instruction {
    @Override
    public void execute(Frame frame) {
        int val = frame.localVars.getInt(1);
        frame.operandStack.pushInt(val, Slot.INT);
    }

    @Override
    public String debug(String prefix) {
        return "iload_1";
    }
}
