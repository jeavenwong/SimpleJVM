package com.jeaven.instruction;

import com.jeaven.rtda.Frame;

// 变量的增指令
public class iincInst implements Instruction {
    private int index;
    private int delta;

    public iincInst(int index, int delta) {
        this.delta = delta;
        this.index = index;
    }

    @Override
    public void execute(Frame frame) {
        int temp = frame.localVars.getInt(index);
        temp += delta;
        frame.localVars.setInt(index, temp);
    }

    // 为什么 offset 是 3？答案参考下面文章
    // https://www.cnblogs.com/niugang0920/p/12424671.html
    @Override
    public int offset() {
        return 3;
    }

    @Override
    public String debug(String prefix) {
        return "iinc";
    }
}
