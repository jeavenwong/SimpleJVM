package com.jeaven.instruction;

import com.jeaven.rtda.Frame;

// 栈顶数存入局部变量表指令
public class istore1Inst implements Instruction {
    @Override
    public void execute(Frame frame) {
        int var = frame.operandStack.popInt();
        frame.localVars.setInt(1, var);
    }

    @Override
    public String debug(String prefix) {
        return "istore_1";
    }
}
