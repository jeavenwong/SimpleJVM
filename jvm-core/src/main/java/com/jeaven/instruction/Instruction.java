package com.jeaven.instruction;

import com.jeaven.rtda.Frame;

// 字节码指令的公共接口
// 关于 JVM 的字节码的详细文档可以参考下面的文章
// https://blog.csdn.net/weixin_40234548/article/details/81533673
public interface Instruction {
    default int offset() {
        return 1;
    }

    void execute(Frame frame);

    default String format() {
        return this.getClass().getSimpleName();
    }

    default String debug(String prefix) {
        return prefix + " " + this.format();
    }
}
