package com.jeaven.instruction;

import com.jeaven.rtda.Frame;

// 空指令
public class NopInst implements Instruction {
    @Override
    public void execute(Frame frame) {
        System.out.println("这个是空指令");
    }

    @Override
    public String debug(String prefix) {
        return "空指令...";
    }
}
