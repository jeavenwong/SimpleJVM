package com.jeaven.instruction;

import com.jeaven.rtda.Frame;

// 获得静态变量
public class getstaticInst implements Instruction {
    @Override
    public void execute(Frame frame) {
        // todo
    }

    @Override
    public int offset() {
        return 3;
    }

    @Override
    public String debug(String prefix) {
        return "getstatic [Note]:getstaticInst还没有实现";
    }
}
