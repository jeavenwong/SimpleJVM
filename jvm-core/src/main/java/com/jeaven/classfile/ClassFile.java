package com.jeaven.classfile;

import com.jeaven.classfile.attribute.Attribute;
import com.jeaven.classfile.attribute.Attributes;
import com.jeaven.classfile.attribute.BootstrapMethods;
import com.jeaven.classfile.attribute.SourceFile;
import com.jeaven.classfile.constantpool.ConstantPool;
import com.jeaven.classfile.fields.Fields;
import com.jeaven.classfile.interfaces.Interfaces;
import com.jeaven.classfile.methods.Methods;

public class ClassFile {
    public final int magic;
    public final int minorVersion;
    public final int majorVersion;
    public final int constantPoolCount;
    public final ConstantPool cpInfo;
    public final int accessFlags;
    public final int thisClass;
    public final int superClass;
    public final int interfacesCount;
    public final Interfaces interfaces;
    public final int fieldsCount;
    public final Fields fields;
    public final int methodsCount;
    public final Methods methods;
    public final int attributesCount;
    public final Attributes attributes;

    public String source;

    public ClassFile(int magic, int minorVersion, int majorVersion, int constantPoolCount, ConstantPool cpInfo, int accessFlags, int thisClass, int superClass,
                     int interfacesCount, Interfaces interfaces, int fieldsCount, Fields fields, int methodsCount, Methods methods, int attributesCount, Attributes attributes) {
        this.magic = magic;
        this.minorVersion = minorVersion;
        this.majorVersion = majorVersion;
        this.constantPoolCount = constantPoolCount;
        this.cpInfo = cpInfo;
        this.accessFlags = accessFlags;
        this.thisClass = thisClass;
        this.superClass = superClass;
        this.interfacesCount = interfacesCount;
        this.interfaces = interfaces;
        this.fieldsCount = fieldsCount;
        this.fields = fields;
        this.methodsCount = methodsCount;
        this.methods = methods;
        this.attributesCount = attributesCount;
        this.attributes = attributes;
    }

    public void setSource(String source) {
        this.source = source;
    }

    public String getSource() {
        return this.source;
    }

    @Override
    public String toString() {
        return "ClassFile{" +
                "\n, magic=" + magic +
                "\n, minorVersion=" + minorVersion +
                "\n, majorVersion=" + majorVersion +
                "\n, constantPoolCount=" + constantPoolCount +
                "\n, cpInfo=" + cpInfo +
                "\n, accessFlags=" + accessFlags +
                "\n, thisClass=" + thisClass +
                "\n, superClass=" + superClass +
                "\n, interfacesCount=" + interfacesCount +
                "\n, interfaces=" + interfaces +
                "\n, fieldsCount=" + fieldsCount +
                "\n, fields=" + fields +
                "\n, methodsCount=" + methodsCount +
                "\n, methods=" + methods +
                "\n, attributesCount=" + attributesCount +
                "\n, attributes=" + attributes +
                "\n}";
    }


    public String getSourceFile() {
        for (Attribute attribute : this.attributes.attributes) {
            if (attribute instanceof SourceFile) {
                return ((SourceFile) attribute).name;
            }
        }
        return "unknown";
    }

    public BootstrapMethods getBootstrapMethods() {
        for (Attribute attribute : attributes.attributes) {
            if (attribute instanceof BootstrapMethods) {
                return (BootstrapMethods) attribute;
            }
        }
        return null;
    }
}
