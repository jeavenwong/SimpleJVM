package com.jeaven.classfile.fields;

public class Descriptor {
    public final String descriptor;

    public Descriptor(String descriptor) {
        this.descriptor = descriptor;
    }
}
