package com.jeaven.classfile.constantpool.content;

import com.jeaven.classfile.constantpool.ConstantPoolInfo;

public class MethodType extends ConstantPoolInfo {
    public final int descriptionIndex;
    public MethodType(int infoEnum, int descriptionIndex) {
        super(infoEnum);
        this.descriptionIndex = descriptionIndex;
    }
}
