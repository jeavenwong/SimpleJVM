package com.jeaven.classfile.constantpool.content;

import com.jeaven.classfile.constantpool.ConstantPoolInfo;

public class NameAndType extends ConstantPoolInfo {
    public final int nameIndex;
    public final int descriptionIndex;
    public NameAndType(int infoEnum, int nameIndex, int descriptionIndex) {
        super(infoEnum);
        this.nameIndex = nameIndex;
        this.descriptionIndex = descriptionIndex;
    }

}
