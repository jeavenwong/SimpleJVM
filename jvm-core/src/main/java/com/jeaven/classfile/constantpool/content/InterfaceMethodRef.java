package com.jeaven.classfile.constantpool.content;

import com.jeaven.classfile.constantpool.ConstantPoolInfo;

public class InterfaceMethodRef extends ConstantPoolInfo {
    public final int classIndex;
    public final int nameAndTypeIndex;

    public InterfaceMethodRef(int indexEnum, int classIndex, int nameAndTypeIndex) {
        super(indexEnum);
        this.classIndex = classIndex;
        this.nameAndTypeIndex = nameAndTypeIndex;
    }
}
