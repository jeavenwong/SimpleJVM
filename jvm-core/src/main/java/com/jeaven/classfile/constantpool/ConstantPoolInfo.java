package com.jeaven.classfile.constantpool;

public class ConstantPoolInfo {
    public final int infoEnum;  // 这个是判断常量池中数据类型的，每一个类型都有，于是单独建立一个类，常量池内容继承此类即可

    public ConstantPoolInfo(int infoEnum) {
        this.infoEnum = infoEnum;
    }
}
