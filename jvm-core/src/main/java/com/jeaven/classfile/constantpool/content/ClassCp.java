package com.jeaven.classfile.constantpool.content;

import com.jeaven.classfile.constantpool.ConstantPoolInfo;

public class ClassCp extends ConstantPoolInfo {
    public final int nameIndex;

    public ClassCp(int infoEnum, int nameIndex) {
        super(infoEnum);
        this.nameIndex = nameIndex;
    }
}
