package com.jeaven.classfile.constantpool.content;

import com.jeaven.classfile.constantpool.ConstantPoolInfo;

public class StringCp extends ConstantPoolInfo {
    public final int stringIndex;
    public StringCp(int infoEnum, int stringIndex) {
        super(infoEnum);
        this.stringIndex = stringIndex;
    }
}
