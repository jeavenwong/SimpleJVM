package com.jeaven.classfile.constantpool.content;

import com.jeaven.classfile.constantpool.ConstantPoolInfo;

public class DoubleCp extends ConstantPoolInfo {
    public final double val;
    public DoubleCp(int infoEnum, double val) {
        super(infoEnum);
        this.val = val;
    }
}
