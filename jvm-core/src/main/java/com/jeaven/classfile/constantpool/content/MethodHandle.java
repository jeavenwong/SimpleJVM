package com.jeaven.classfile.constantpool.content;

import com.jeaven.classfile.constantpool.ConstantPoolInfo;

public class MethodHandle extends ConstantPoolInfo {
    public final int referenceKind;
    public final int referenceIndex;
    public MethodHandle(int infoEnum, int referenceKind, int referenceIndex) {
        super(infoEnum);
        this.referenceKind = referenceKind;
        this.referenceIndex = referenceIndex;
    }

}
