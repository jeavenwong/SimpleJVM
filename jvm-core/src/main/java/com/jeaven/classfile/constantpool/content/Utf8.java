package com.jeaven.classfile.constantpool.content;

import com.jeaven.classfile.constantpool.ConstantPoolInfo;

import java.io.ByteArrayInputStream;

public class Utf8 extends ConstantPoolInfo {
    public final byte[] bytes;

    public Utf8(int infoEnum, byte[] bytes) {
        super(infoEnum);
        this.bytes = bytes;
    }

//    1. java 的 classfile 中 constant_utf8_info 采用的字符串编码是 UTF8缩略编码
//    2. java 采用 big endian 编码
//    2. 下面的函数是是将缩略 utf8 编码转换为 java 的字符串
//    3. 虽然没怎么看明白，但是只要是全英文，都只有 b > 0 的情况
    public final String getString() {
        ByteArrayInputStream is = new ByteArrayInputStream(bytes);
        StringBuilder sb = new StringBuilder();
        while (is.available() > 0) {
            int b = is.read();
            if (b > 0) {
                sb.append((char) b);
            } else {
                int b2 = is.read();
                if ((b2 & 0xf0) != 0xe0) {
                    sb.append((char) ((b & 0x1F) << 6 | b2 & 0x3F));
                } else {
                    int b3 = is.read();
                    sb.append((char) ((b & 0x0F) << 12 | (b2 & 0x3F) << 6 | b3 & 0x3F));
                }
            }
        }
        return sb.toString();
    }

}
