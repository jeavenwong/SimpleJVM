package com.jeaven.classfile.constantpool.content;

import com.jeaven.classfile.constantpool.ConstantPoolInfo;

public class LongCp extends ConstantPoolInfo {
    public final long val;
    public LongCp(int infoEnum, long val) {
        super(infoEnum);
        this.val = val;
    }
}
