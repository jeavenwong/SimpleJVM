package com.jeaven.classfile.constantpool.content;

import com.jeaven.classfile.constantpool.ConstantPoolInfo;

public class IntegerCp extends ConstantPoolInfo {
    public final int val;

    public IntegerCp(int infoEnum, int val) {
        super(infoEnum);
        this.val = val;
    }
}
