package com.jeaven.classfile.constantpool.content;

import com.jeaven.classfile.constantpool.ConstantPoolInfo;

public class FieldRef extends ConstantPoolInfo {
    public final int classIndex;
    public final int nameAndTypeIndex;

    public FieldRef(int infoEnum, int classIndex, int nameAndTypeIndex) {
        super(infoEnum);
        this.classIndex = classIndex;
        this.nameAndTypeIndex = nameAndTypeIndex;
    }
}
