package com.jeaven.classfile.constantpool.content;

import com.jeaven.classfile.constantpool.ConstantPoolInfo;

public class FloatCp extends ConstantPoolInfo {
    public final float val;
    public FloatCp(int infoEnum, float val) {
        super(infoEnum);
        this.val = val;
    }
}
