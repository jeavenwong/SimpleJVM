package com.jeaven.classfile.constantpool.content;

import com.jeaven.classfile.constantpool.ConstantPoolInfo;

public class InvokeDynamic extends ConstantPoolInfo {
    //CONSTANT_InvokeDynamic_info {
    //    u1 tag;
    //    u2 bootstrap_method_attr_index;
    //    u2 name_and_type_index;
    //}
    public final int bootstrapMethodAttrIndex;
    public final int nameAndTypeIndex;
    public InvokeDynamic(int infoEnum, int bootstrapMethodAttrIndex, int nameAndTypeIndex) {
        super(infoEnum);
        this.bootstrapMethodAttrIndex = bootstrapMethodAttrIndex;
        this.nameAndTypeIndex = nameAndTypeIndex;
    }
}
