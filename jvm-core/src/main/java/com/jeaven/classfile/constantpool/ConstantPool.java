package com.jeaven.classfile.constantpool;

// 这个类是 Constant Pool 常量池
public class ConstantPool {
    public final ConstantPoolInfo[] infos;

    public ConstantPool(int count) {
        this.infos = new ConstantPoolInfo[count];
    }
}
