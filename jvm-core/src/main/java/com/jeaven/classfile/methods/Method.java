package com.jeaven.classfile.methods;

import com.jeaven.classfile.attribute.*;
import com.jeaven.classfile.fields.Descriptor;

//method_info {
//    u2             access_flags;
//    u2             name_index;
//    u2             descriptor_index;
//    u2             attributes_count;
//    attribute_info attributes[attributes_count];
//    }
public class Method {
    public final int accessFlags;
    public final String name;
    public final Descriptor descriptor;
    public final Attributes attributes;

    public Method(int accessFlags, String name, Descriptor descriptor, Attributes attributes) {
        this.accessFlags = accessFlags;
        this.name = name;
        this.descriptor = descriptor;
        this.attributes = attributes;
    }

    public Code getCode() {
        for(Attribute attribute : this.attributes.attributes) {
            if(attribute instanceof Code) {
                return (Code) attribute;
            }
        }
        return null;
    }

    public LineNumerTable getLineNumber() {
        if(getCode() == null) {
            return null;
        }
        for(Attribute attribute : this.attributes.attributes) {
            if(attribute instanceof LineNumerTable) {
                return (LineNumerTable) attribute;
            }
        }
        return null;
    }
}
