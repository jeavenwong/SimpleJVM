package com.jeaven.classfile.interfaces;

public class Interfaces {
    public final Interface[] interfaces;

    public Interfaces(Interface[] interfaces) {
        this.interfaces = interfaces;
    }
}
