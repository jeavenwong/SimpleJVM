package com.jeaven.classfile.interfaces;

public class Interface {
    public final String name;

    public Interface(String name) {
        this.name = name;
    }

    public String getName() {
        return name;
    }
}
