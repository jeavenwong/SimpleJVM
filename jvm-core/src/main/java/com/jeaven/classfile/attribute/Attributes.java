package com.jeaven.classfile.attribute;

public class Attributes {
    public final Attribute[] attributes;

    public Attributes(Attribute[] attributes) {
        this.attributes = attributes;
    }

    public Attributes(int attributeCount) {
        this.attributes = new Attribute[attributeCount];
    }
}
