package com.jeaven.classfile.attribute;

public class ExceptionTable {
    public final Exception[] exceptions;

    public ExceptionTable(Exception[] exceptions) {
        this.exceptions = exceptions;
    }
}
