package com.jeaven.classfile.attribute;

public class ConstantValue extends Attribute {
    //  onstantValue_attribute {
    //    u2 attribute_name_index;
    //    u4 attribute_length;
    //    u2 constantvalue_index;
    //  }

    public final Object val;

    public ConstantValue(Object val) {
        this.val = val;
    }
}
