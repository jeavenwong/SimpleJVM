package com.jeaven.classfile.attribute;

public class BootstrapMethods extends Attribute {
    public final BootstrapMethod[] bootstrapMethods;

    public BootstrapMethods(BootstrapMethod[] bootstrapMethods) {
        this.bootstrapMethods = bootstrapMethods;
    }

}
