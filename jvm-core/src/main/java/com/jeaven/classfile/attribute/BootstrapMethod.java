package com.jeaven.classfile.attribute;

public class BootstrapMethod {
    public final Integer bootstrapMethodRefInx;
    public final Integer[] argRefs;

    public BootstrapMethod(Integer bootstrapMethodRefInx, Integer[] argRefs) {
        this.bootstrapMethodRefInx = bootstrapMethodRefInx;
        this.argRefs = argRefs;
    }
}
