package com.jeaven.classfile.attribute;

import com.jeaven.instruction.Instruction;

import java.util.Map;

public class CodeFromByte {
    private final Map<Integer, Instruction> instructions;

    public CodeFromByte(Map<Integer, Instruction> instructions) {
        this.instructions = instructions;
    }

    public Instruction getInstruction(int pc) {
        return instructions.get(pc);
    }
}
