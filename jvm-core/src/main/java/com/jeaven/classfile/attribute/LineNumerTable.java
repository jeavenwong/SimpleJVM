package com.jeaven.classfile.attribute;

public class LineNumerTable extends Attribute {
    //  LineNumberTable_attribute {
    //    u2 attribute_name_index;
    //    u4 attribute_length;
    //    u2 line_number_table_length;
    //    {   u2 start_pc;
    //      u2 line_number;
    //    } line_number_table[line_number_table_length];
    //  }

    public final Line[] lines;

    public LineNumerTable(Line[] lines) {
        this.lines = lines;
    }
}
