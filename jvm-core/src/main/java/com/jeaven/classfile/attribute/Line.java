package com.jeaven.classfile.attribute;

public class Line {
    public final int startPC;
    public final int lineNum;

    public Line(int startPC, int lineNum) {
        this.lineNum = lineNum;
        this.startPC = startPC;
    }
}
