package com.jeaven.classfile.attribute;

public class SourceFile extends Attribute {
    //SourceFile_attribute {
    //    u2 attribute_name_index;
    //    u4 attribute_length;
    //    u2 sourcefile_index;
    //  }

    public final String name;

    public SourceFile(String name) {
        this.name = name;
    }
}

