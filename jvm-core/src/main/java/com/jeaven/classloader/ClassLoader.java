package com.jeaven.classloader;

import com.jeaven.classfile.ClassFile;
import com.jeaven.rtda.heap.KClass;

import java.io.IOException;

// 类加载器接口
public interface ClassLoader {
    void loadClass(String className) throws IOException;
    KClass doLoadClass(String name, ClassFile classFile);
    void registerClass(String className, KClass kClass);
}
