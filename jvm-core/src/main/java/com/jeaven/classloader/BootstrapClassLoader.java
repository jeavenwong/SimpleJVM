package com.jeaven.classloader;

import com.jeaven.VirtualMachine;
import com.jeaven.classfile.ClassFile;
import com.jeaven.classfile.attribute.BootstrapMethods;
import com.jeaven.classfile.attribute.Code;
import com.jeaven.classfile.fields.Field;
import com.jeaven.classfile.interfaces.Interface;
import com.jeaven.classfile.methods.Method;
import com.jeaven.classpath.ClassPath;
import com.jeaven.rtda.Slot;
import com.jeaven.rtda.heap.Heap;
import com.jeaven.rtda.heap.KClass;
import com.jeaven.rtda.heap.KField;
import com.jeaven.rtda.heap.KMethod;
import com.jeaven.utils.Utils;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

// 启动类加载器
public class BootstrapClassLoader implements ClassLoader {
    public BootstrapClassLoader() {
        super();
    }

    @Override
    public void loadClass(String className) throws IOException {
        KClass cache = VirtualMachine.heap.findClass(className);
        if(cache != null) {
            return;
        }
        ClassFile classFile = ClassPath.findClass0(className);
        KClass kClass = doLoadClass(className, classFile);
        className = className.replace(".", "/");
        registerClass(className, kClass);
    }

    // 装载系统初始的类
    public void loadPrimitiveClass(String className) {
        KClass cache = VirtualMachine.heap.findClass(className);
        if(cache != null) {
            return;
        }
        KClass kClass = new KClass(1, className, this);

        registerClass(className, kClass);
    }

    @Override
    public KClass doLoadClass(String name, ClassFile classFile) {
        List<KMethod> methods = new ArrayList<>();

        for (Method method : classFile.methods.methods) {
            // 将 classfile 中的 method 转换为 kmethod
            Code code = method.getCode();
            if (code == null) {
                methods.add(new KMethod(method.accessFlags, method.name, method.descriptor.descriptor, 0, 0,
                        null, null, method.getLineNumber()));
            } else {
                methods.add(new KMethod(method.accessFlags, method.name, method.descriptor.descriptor,
                        code.maxStacks, code.maxLocals, code.getInstructions(), code.exceptionTable,
                        method.getLineNumber()));
            }
        }

        List<KField> fields = new ArrayList<>();
        for (Field field : classFile.fields.fields) {
            // 将 classfile 中的 field 转换为 kfield
            fields.add(new KField(field.accessFlags, field.name, field.descriptor.descriptor));
        }

        // field interfaceInit 分配存储空间
        for (KField it : fields) {
            switch (it.descriptor) {
                case "Z":
                case "C":
                case "B":
                case "S":
                case "I":
                    it.val = new Slot[]{new Slot(0, Slot.INT)};
                    break;
                case "F":
                    it.val = new Slot[]{new Slot(0, Slot.FLOAT)};
                    break;
                case "D":
                    it.val = new Slot[]{new Slot(0, Slot.DOUBLE_HIGH), new Slot(0, Slot.DOUBLE_LOW)};
                    break;
                case "J":
                    it.val = new Slot[]{new Slot(0, Slot.LONG_HIGH), new Slot(0, Slot.LONG_LOW)};
                    break;
                default:
                    it.val = new Slot[]{new Slot(null)};
                    break;
            }
        }

        int scIdx = classFile.superClass;
        String superClassName = null;
        if (scIdx != 0) {
            superClassName = Utils.getClassName(classFile.cpInfo, scIdx);
        }

        List<String> interfaceNames = new ArrayList<>();
        if (classFile.interfaces.interfaces.length != 0) {
            for (Interface anInterface : classFile.interfaces.interfaces) {
                interfaceNames.add(anInterface.getName());
            }
        }

        BootstrapMethods bootstrapMethods = classFile.getBootstrapMethods();

        return new KClass(classFile.accessFlags, name, superClassName, interfaceNames, methods, fields,
                bootstrapMethods, classFile.cpInfo, this, classFile);
    }

    @Override
    public void registerClass(String className, KClass kClass) {
        VirtualMachine.heap.registerClass(className, kClass);
    }
}
