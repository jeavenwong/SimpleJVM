package com.jeaven;

import java.util.Arrays;

/**
 * Hello world!
 *
 */
public class App {
    public static void main(String[] args) {
        args = new String[]{"com.jeaven.test.Test"}; // debug

        if(args.length == 0) {
            System.out.println("usage: java [options] class [args]\\n");
            return;
        }

         Args cmd = Args.parseArgs(args);

        if(cmd.version) {
            System.out.println("openjdk version '1.8.0_252'\n" +
                    "OpenJDK Runtime Environment (Zulu 8.46.0.19-CA-win64) (build 1.8.0_252-b14)\n" +
                    "OpenJDK 64-Bit Server VM (Zulu 8.46.0.19-CA-win64) (build 25.252-b14, mixed mode)\n");
            return;
        }

        // debug
       // debug(cmd);

        // JVM 运行指令
        VirtualMachine vm = new VirtualMachine();
        try {
            vm.run(cmd);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    // debug
    public static void debug(Args cmd) {
        System.out.println("classpath:\t" + cmd.classpath);
        System.out.println("clazz:\t" + cmd.clazz);
        System.out.println("args:\t" + Arrays.toString(cmd.args));
    }
}
