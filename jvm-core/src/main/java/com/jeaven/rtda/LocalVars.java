package com.jeaven.rtda;

public class LocalVars {
    private final Slot[] slots;

    public LocalVars(int maxLocals) {
        slots = new Slot[maxLocals];
    }

    public String debug() {
        StringBuffer sb = new StringBuffer("LocalVars: [ ");
        for(Slot slot : slots) {
            sb.append(slot.toString());
        }
        sb.append(" ]");
        return sb.toString();
    }

    public void setInt(int index, Integer val) {
        slots[index] = new Slot(val, Slot.INT);
    }

    public Integer getInt(int index) {
        return slots[index].num;
    }

    public void setFloat(int index, Float val) {
        slots[index] = new Slot(Float.floatToIntBits(val), Slot.FLOAT);
    }

    public Float getFloat(int index) {
        return Float.intBitsToFloat(slots[index].num);
    }

    public void setLong(int index, Long val) {
        int low = (int) (val & 0x00000000ffffffffL);
        int high = (int) (val >> 32);
        slots[index] = new Slot(low, Slot.LONG_LOW);
        slots[index+1] = new Slot(high, Slot.LONG_HIGH);
    }

    public Long getLong(int index) {
        int low = slots[index].num;
        int high = slots[index+1].num;
        Long llow = (low & 0x00000000ffffffffL);
        Long lhigh = (low & 0x00000000ffffffffL) << 32;
        return llow | lhigh;
    }

    public void setDouble(int index, Double val) {
        Long temp = Double.doubleToLongBits(val);
        setLong(index, temp);
    }

    public Double getDouble(int index) {
        Long temp = getLong(index);
        return Double.longBitsToDouble(temp);
    }

    public void setRef(int index, Object obj) {
        slots[index] = new Slot(obj);
    }

    public Object getRef(int index) {
        return slots[index].ref;
    }

}
