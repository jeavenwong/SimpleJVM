package com.jeaven.rtda;

import com.jeaven.rtda.heap.KMethod;

public class Frame {
    public final LocalVars localVars;
    public final OperandStack operandStack;
    public final Thread thread;
    public Integer nextPC;

    public Frame(int maxLocals, int maxStackSize, Thread thread) {
        this.localVars = new LocalVars(maxLocals);
        this.operandStack = new OperandStack(maxStackSize);
        this.thread = thread;
        this.nextPC = 0;
    }

    public Frame(KMethod kMethod, Thread thread) {
        int maxLocals = kMethod.maxLocals;
        int maxStackSize = kMethod.maxStacks;
        this.localVars = new LocalVars(maxLocals);
        this.operandStack = new OperandStack(maxStackSize);
        this.thread = thread;
        this.nextPC = 0;
    }

    public String debug() {
        String pcStr = "next PC is:\t" + nextPC;
        String localVarsStr = "LocalVars is:\t" + localVars.debug();
        String operandStackStr = "OperandStack is:\t" + operandStack.debug();
        System.out.println(pcStr);
        System.out.println(localVarsStr);
        System.out.println(operandStackStr);
        return pcStr + "\t" + localVarsStr + "\t" + operandStackStr;
    }

    public void setRef(Integer index, Object ref) {
        this.localVars.setRef(index, ref);
    }

    public Object getRef(Integer index) {
        return this.localVars.getRef(index);
    }

}
