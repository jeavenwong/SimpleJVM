package com.jeaven.rtda.heap;

import java.util.List;

public class KLambdaObject extends KObject {
    public final List<Object> args;

    public KLambdaObject(KClass clazz, List<Object> args) {
        super(clazz);
        this.args = args;
    }
}
