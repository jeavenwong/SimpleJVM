package com.jeaven.rtda.heap;

import com.jeaven.rtda.Slot;

public class KField {
    public final int accessFlags;
    public final String name;
    public final String descriptor;

    public Slot[] val;

    public KField(int accessFlags, String name, String descriptor) {
        this.accessFlags = accessFlags;
        this.name = name;
        this.descriptor = descriptor;
    }

    public KField(int accessFlags, String name, String descriptor, Slot[] val) {
        this.accessFlags = accessFlags;
        this.name = name;
        this.descriptor = descriptor;
        this.val = val;
    }

    public boolean isStatic() { // static 访问符的字节码标志值是 0x08
        return (accessFlags & 0x00000008) != 0x00000000;
    }

}
