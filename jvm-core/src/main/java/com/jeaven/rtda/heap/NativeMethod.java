package com.jeaven.rtda.heap;

import com.jeaven.rtda.Frame;

public interface NativeMethod {
    void invoke(Frame frame);

    default String debug(String prefix) {
        return prefix + " 是个本地方法...";
    }
}
