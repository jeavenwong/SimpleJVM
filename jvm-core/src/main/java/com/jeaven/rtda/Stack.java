package com.jeaven.rtda;

public class Stack<E> extends java.util.Stack<E> {
    private int maxStackSize;

    public Stack(int maxStackSize) {
        this.maxStackSize = maxStackSize;
    }

    @Override
    public E push(E item) {
        if(this.size() >= maxStackSize) {
            throw new StackOverflowError("current stack is overflow...");
        }
        return super.push(item);
    }

    @Override
    public synchronized E pop() {
        if(this.size() <= 0) {
            throw new IllegalStateException();
        }
        return super.pop();
    }
}
