package com.jeaven.rtda;

public class Thread {
    private Integer pc;
    private Stack<Frame> stack;

    public Thread(int maxStackSize) {
        this.pc = 0;
        this.stack = new Stack<>(maxStackSize);
    }

    public void setPc(Integer pc) {
        this.pc = pc;
    }

    public Integer getPc() {
        return pc;
    }

    public void pushFrame(Frame frame) {
        this.stack.push(frame);
    }

    public Frame popFrame() {
        return this.stack.pop();
    }

    public Frame currentFrame() {
        return this.stack.peek();
    }

    public String debug() {
        StringBuffer sb = new StringBuffer("Thread: Frame: [ ");
        for(Frame frame : stack) {
            sb.append(frame.debug());
        }
        sb.append(" ]");
        return sb.toString();
    }

}
