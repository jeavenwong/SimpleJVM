package com.jeaven;

public class Args {
    public String classpath;
    public boolean version;
    public String clazz;
    public String[] args;

    public Args() {
        version = false;
        clazz = null;
        classpath = null;
        args = null;
    }

    public static Args parseArgs(String... args) {
        Args cmd  = new Args();

        if("-version".equals(args[0])) {
             cmd.version = true;
             return cmd;
        }

        if("-cp".equals(args[0])) {
            cmd.classpath = args[1];
            cmd.clazz = args[2];
            if(args.length > 3) {
                String[] programArgs = new String[args.length - 3];
                for(int i = 3; i < args.length; i++) {
                    programArgs[i-3] = args[i];
                }
                cmd.args = programArgs;
            }
            return cmd;
        }

        cmd.clazz = args[0];
        //cmd.classpath = null;
        if(args.length > 3) {
            String[] programArgs = new String[args.length - 3];
            for(int i = 3; i < args.length; i++) {
                programArgs[i-3] = args[i];
            }
            cmd.args = programArgs;
        }
        return cmd;
    }
}
