package com.jeaven.nativeBridge.java.lang;

import com.jeaven.rtda.heap.Heap;

public class StringBridge {  // String 的 native 方法的交接实现
    public static void registerNatives0() {
        // String intern()
        Heap.registerMethod("java/lang/String_intern_()Ljava/lang/String", (frame)->{
            System.out.println("this is the native method intern() of the String class");
        });
    }
}
