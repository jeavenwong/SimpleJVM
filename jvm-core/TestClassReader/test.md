#### 测试 ClassReader

Date: 2020/06/07

使用测试文件: 

- 此文件夹下  Test.java Test.class  InterfaceTest.class

测试结果：

- 和 javap 解析结果保持一致

测试结果比对如下图：

![testFigure](./testRes.png)



