package com.jeaven.interpreter.stackBased;

import com.jeaven.interpreter.instruction.Instruction;

// push 1 --- 将 1 push 到 stack 中
public class PushInst implements Instruction {
    private Integer i;
    public PushInst(Integer _i) {
        i = _i;
    }

    public void execute() {
        StackBasedInterpreter.opStack.push(this.i);
    }

}
