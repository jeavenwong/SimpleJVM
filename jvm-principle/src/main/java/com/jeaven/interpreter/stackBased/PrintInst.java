package com.jeaven.interpreter.stackBased;

import com.jeaven.interpreter.instruction.Instruction;

// print --- 打印栈顶的元素
public class PrintInst implements Instruction {
    public PrintInst() {}

    public void execute() {
        Integer print_a = StackBasedInterpreter.opStack.pop();
        System.out.println(print_a);
    }
}
