package com.jeaven.interpreter.stackBased;

import com.jeaven.interpreter.instruction.Instruction;

// add --- 从栈顶pop两个数相加再push
public class AddInst implements Instruction {
    public AddInst() { }

    public void execute() {
        Integer op_a = StackBasedInterpreter.opStack.pop();
        Integer op_b = StackBasedInterpreter.opStack.pop();
        StackBasedInterpreter.opStack.push(op_a + op_b);
    }


}
