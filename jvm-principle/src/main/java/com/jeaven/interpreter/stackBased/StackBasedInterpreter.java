package com.jeaven.interpreter.stackBased;

import com.jeaven.interpreter.instruction.Instruction;

import java.util.Arrays;
import java.util.List;
import java.util.Stack;

// stack-based interpreter demo
public class StackBasedInterpreter {
    public StackBasedInterpreter() { }
    public static final Stack<Integer> opStack = new Stack<Integer>();
    private List<Instruction> instList;

    public void run() {
        instList = getInsts();
        int pc = 0;
        int size = instList.size();
        while(pc < size) {
            Instruction inst = instList.get(pc);
            inst.execute();
            pc++;
        }
    }

    public List<Instruction> getInsts() {
        return Arrays.asList(
                new PushInst(1),
                new PushInst(1),
                new AddInst(),
                new PrintInst()
        );
    }

    // test
    public static void main(String[] args) {
        new StackBasedInterpreter().run();
    }
}
