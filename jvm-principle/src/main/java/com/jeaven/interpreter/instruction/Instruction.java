package com.jeaven.interpreter.instruction;

public interface Instruction {
    void execute();
}