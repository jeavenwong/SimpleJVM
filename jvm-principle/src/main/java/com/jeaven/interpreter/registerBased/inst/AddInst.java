package com.jeaven.interpreter.registerBased.inst;

import com.jeaven.interpreter.instruction.Instruction;
import com.jeaven.interpreter.registerBased.RegisterBasedInterpreter;

// add &1 &2 --- 把 register2 的值加到 register1 中
public class AddInst implements Instruction {
    private Integer addr1;
    private Integer addr2;
    public AddInst(Integer _addr1, Integer _addr2) {
        addr1 = _addr1;
        addr2 = _addr2;
    }
    public void execute() {
        Integer a = RegisterBasedInterpreter.opRegister.get(addr1);
        Integer b = RegisterBasedInterpreter.opRegister.get(addr2);
        a = a + b;
        RegisterBasedInterpreter.opRegister.put(addr1, a);
    }
}
