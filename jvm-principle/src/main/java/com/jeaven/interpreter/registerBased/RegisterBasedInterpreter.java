package com.jeaven.interpreter.registerBased;

import com.jeaven.interpreter.registerBased.inst.AddInst;
import com.jeaven.interpreter.registerBased.inst.PrintInst;
import com.jeaven.interpreter.instruction.Instruction;
import com.jeaven.interpreter.registerBased.inst.MovInst;

import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class RegisterBasedInterpreter {
    public RegisterBasedInterpreter() { }

    // 用 Map<Integer, Integer> 来模拟无限组寄存器(2^32个寄存器)
    // 第一个下标为寄存器的地址
    // 第二个下标为寄存器的长度(int为32bit)
    public static final Map<Integer, Integer> opRegister = new HashMap<Integer, Integer>();

    private List<Instruction> instList;

    public void run() {
        instList = getInsts();
        int pc = 0;
        int size = instList.size();
        while(pc < size) {
            Instruction inst = instList.get(pc);
            inst.execute();
            pc++;
        }
    }

    public List<Instruction> getInsts() {
        return Arrays.asList(
                new MovInst(0,1),
                new MovInst(1,1),
                new AddInst(0, 1),
                new PrintInst(0)
        );
    }

}

