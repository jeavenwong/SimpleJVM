package com.jeaven.interpreter.registerBased.inst;

import com.jeaven.interpreter.instruction.Instruction;
import com.jeaven.interpreter.registerBased.RegisterBasedInterpreter;

// print &1 --- 打印 register1 的值
public class PrintInst implements Instruction {
    private Integer addr;
    public PrintInst(Integer _addr) {
        addr = _addr;
    }
    public void execute() {
        Integer a = RegisterBasedInterpreter.opRegister.get(addr);
        System.out.println(a);
    }
}
