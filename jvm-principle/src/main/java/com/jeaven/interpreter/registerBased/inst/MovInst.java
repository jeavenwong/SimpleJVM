package com.jeaven.interpreter.registerBased.inst;

import com.jeaven.interpreter.instruction.Instruction;
import com.jeaven.interpreter.registerBased.RegisterBasedInterpreter;

// mov &1 1 --- 将 1 存入 regiter1 中
public class MovInst implements Instruction {
    private Integer addr;
    private Integer num;
    public MovInst(Integer _addr, Integer _num) {
        addr = _addr;
        num = _num;
    }
    public void execute() {
        RegisterBasedInterpreter.opRegister.put(addr, num);
    }

}
