package com.jeaven.interpreter.hybridBased.inst;

import com.jeaven.interpreter.hybridBased.HybridBasedInterpreter;
import com.jeaven.interpreter.instruction.Instruction;

// add --- 从stack pop 两个数进行相加，再将结果 push 到 stack
public class AddInst implements Instruction {
    public AddInst() { }
    public void execute() {
        Integer a = HybridBasedInterpreter.opStack.pop();
        Integer b = HybridBasedInterpreter.opStack.pop();
        Integer res = a + b;
        HybridBasedInterpreter.opStack.push(res);
    }
}
