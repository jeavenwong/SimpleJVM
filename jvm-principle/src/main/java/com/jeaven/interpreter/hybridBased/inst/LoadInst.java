package com.jeaven.interpreter.hybridBased.inst;

import com.jeaven.interpreter.hybridBased.HybridBasedInterpreter;
import com.jeaven.interpreter.instruction.Instruction;

// load &1 --- 从 register1 中取得数字，然后 pop 到 stack 中
public class LoadInst implements Instruction {
    private Integer addr;
    public LoadInst(Integer _addr) {
        addr = _addr;
    }

    public void execute() {
        Integer a = HybridBasedInterpreter.opRegisters.get(addr);
        HybridBasedInterpreter.opStack.push(a);
    }
}
