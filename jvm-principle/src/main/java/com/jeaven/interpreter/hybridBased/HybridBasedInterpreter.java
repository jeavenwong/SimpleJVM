package com.jeaven.interpreter.hybridBased;

import com.jeaven.interpreter.hybridBased.inst.*;
import com.jeaven.interpreter.instruction.Instruction;

import java.util.*;

// 寄存器register和栈stack混合的指令执行的demo
public class HybridBasedInterpreter  {
    public HybridBasedInterpreter() {}
    public static final Map<Integer, Integer> opRegisters = new HashMap<Integer, Integer>();
    public static final Stack<Integer> opStack = new Stack<Integer>();

    public void run() {
        List<Instruction> instList = getInsts();
        int pc = 0;
        int size = instList.size();
        while(pc < size) {
            Instruction inst = instList.get(pc);
            inst.execute();
            pc++;
        }
    }

    public List<Instruction> getInsts() {
        return Arrays.asList(
                new PushInst(1),
                new StoreInst(0),
                new LoadInst(0),
                new PushInst(2),
                new AddInst(),
                new PrintInst()
        );
    }


}
