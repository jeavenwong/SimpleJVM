package com.jeaven.interpreter.hybridBased.inst;

import com.jeaven.interpreter.hybridBased.HybridBasedInterpreter;
import com.jeaven.interpreter.instruction.Instruction;

// push 1 --- 将 1 push 到 stack
public class PushInst implements Instruction {
    private Integer i;
    public PushInst(Integer _i) {
        i = _i;
    }
    public void execute() {
        HybridBasedInterpreter.opStack.push(i);
    }

}
