package com.jeaven.interpreter.hybridBased.inst;

import com.jeaven.interpreter.hybridBased.HybridBasedInterpreter;
import com.jeaven.interpreter.instruction.Instruction;

// store &1 --- 从 stack pop 一个数存到 register1 中
public class StoreInst implements Instruction {
    private Integer addr;
    public StoreInst(Integer _addr) {
        addr = _addr;
    }

    public void execute() {
        Integer a = HybridBasedInterpreter.opStack.pop();
        HybridBasedInterpreter.opRegisters.put(addr, a);
    }
}
