package com.jeaven.interpreter.hybridBased.inst;

import com.jeaven.interpreter.hybridBased.HybridBasedInterpreter;
import com.jeaven.interpreter.instruction.Instruction;

// print --- 从 stack pop 一个数字并且打印到屏幕上
public class PrintInst implements Instruction {
    public PrintInst() {}

    public void execute() {
        Integer a = HybridBasedInterpreter.opStack.pop();
        System.out.println(a);
    }
}
