package com.jeaven.stackBasedInterpreter;

import com.jeaven.interpreter.stackBased.StackBasedInterpreter;
import org.junit.Test;

public class TestStackBasedInterpreter {
    @Test
    public void test() {
        new StackBasedInterpreter().run();
    }
}
