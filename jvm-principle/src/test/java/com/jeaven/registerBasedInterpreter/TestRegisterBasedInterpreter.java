package com.jeaven.registerBasedInterpreter;

import com.jeaven.interpreter.registerBased.RegisterBasedInterpreter;
import org.junit.Test;

public class TestRegisterBasedInterpreter {
    @Test
    public void test() {
        new RegisterBasedInterpreter().run();
    }
}
